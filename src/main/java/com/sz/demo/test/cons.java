package com.sz.demo.test;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class cons {

    @JmsListener(destination = "${topic}")
    public void get(String msg){
        System.out.println(msg);
    }

}
