package com.sz.demo.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.jms.Queue;
import javax.jms.Topic;

@Component
public class pro {

    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    @Autowired
    private Topic topic;

    @Scheduled(fixedDelay = 5000)
    public void set(){
        long l = System.currentTimeMillis();
        System.out.println("发送到队列");
        jmsMessagingTemplate.convertAndSend(topic,l+"");
    }
}
