package com.sz.demo.test;


import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.jms.Topic;

@Component
public class topic {

    @Value("${topic}")
    private String topic;

    @Bean
    public Topic getQueue(){
        return new ActiveMQTopic(topic);
    }
}
